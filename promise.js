// const myPromise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//       resolve(2040);
//     }, 2000);
//   });

const axios = require("axios");

const access_key = "3a3ef2f1cbad883c58b3afa43f1865a0";

const myPromise = new Promise((resolve, reject) => {
    axios.get(`http://1api.exchangeratesapi.io/latest?access_key=${access_key}`)
    .then((response) => {
        resolve(response.data.rates.IDR);
    })
});

myPromise
  .then((value) => {
    console.log(`Value = ${value}`);
  })
  .catch((err) => {
    console.log(err);
  });
