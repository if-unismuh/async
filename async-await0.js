const getCoffee = () => {
    return new Promise((resolve, reject) => {
        const seeds = 100;
        setTimeout(() => {
            if (seeds >= 10) {
                resolve("Kopi didapatkan!");
            } else {
                reject("Biji kopi habis!");
            }
        }, 1000);
    })
}

// const makeCoffee = () => {
//     // async process menggunakan promise
//     const coffee = getCoffee(); 
//     console.log(coffee);
// }
// makeCoffee();

// /* output
// Promise { <pending> }
// */

// Promise
// const makeCoffee = () => {
//     getCoffee().then(coffee => {
//         console.log(coffee);
//     });
// }

// makeCoffee();

// Callback
const makeCoffee = () => {
    getCoffee((coffee) => {
        console.log(coffee);
    });
}

makeCoffee();