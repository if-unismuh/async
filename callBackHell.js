const nyalakanMesin = () => {
    console.log(`Mesin Menyala`);
}

const gilingBijiKopi = () => {
    console.log(`Biji Kopi Sudah digiling`);
}

const panaskanAir = () => {
    console.log(`Air Sudah Panas`);
}

const campurAirKopi = () => {
    console.log(`Kopi Sudah Larut`);
}

const tuangkanSusu = () => {
    console.log(`Gelas Telah Berisi Susu`);
}

const tuangkanKopi = () => {
    console.log(`Mesin Menyala`);
}

const siapkanKopi = () => {
    console.log(`Kopi Anda sudah siap!`);
}

const buatKopiSync = (bijiKopi, air, susu) => {
    const mesin = nyalakanMesin()
    const kopiBubuk = gilingBijiKopi(mesin, bijiKopi)
    const airPanas = panaskanAir(air)
    const kopi = campurAirKopi(kopiBubuk,airPanas)
    kopi = tuangkanKopi(kopi)
    kopi = tuangkanSusu(susu)
    siapkanKopi(kopi)
}

const buatKopiAsync = (bijiKopi, air, susu) => {
    nyalakanMesin(() => {
        gilingBijiKopi(bijiKopi, (kopiBubuk)=>{
            panaskanAir(air, (kopiBubuk,airPanas) => {
                campurAirKopi(kopiBubuk,airPanas, (kopi) =>{
                    tuangkanKopi(kopi, (kopi) =>{
                        tuangkanSusu(susu, (kopi) =>{
                            siapkanKopi(kopi, (kopi) => {
                                console.log(kopi)
                            })
                        })
                    })
                })
            })
        })  
    })
}

