const stock = {
    coffeeBeans: 230,
    water: 1000,
}
 
const checkStock = () => {
    return new Promise((resolve, reject) => {
        if (stock.coffeeBeans >= 16 && stock.water >= 250) {
            resolve("Bahan baku cukup. Bisa membuat kopi");
        } else {
            reject("Bahan baku tidak cukup");
        }
    });
};
 
const handleSuccess = resolvedValue => {
    console.log(resolvedValue);
}
 
const handleFailure = rejectionReason => {
    console.log(rejectionReason);
}
 
// checkStock().then(handleSuccess, handleFailure);

checkStock()
    .then(handleSuccess)
    .then(null, handleFailure);

// checkStock()
//   .then(handleSuccess)
//   .catch(handleFailure);