const proses1 = () => {
    setTimeout(() => {
        console.log("Proses 1 - 4 Detik");
      }, 4000); // proses ditunda selama 20000 miliseconds 
}

const proses2 = () => {
    setTimeout(() => {
        console.log("Proses 2 - 1 Detik");
      }, 1000); // proses ditunda selama 7000 miliseconds 
}

console.time("dbsave");
proses1()
proses2()
console.timeEnd("dbsave");