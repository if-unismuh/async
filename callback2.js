const orderKopi = (callback) => {
  let kopi = null;
  console.log("Sedang membuat kopi, silakan tunggu...");
  setTimeout(() => {
    kopi = "Kopi sudah jadi!";
    callback(kopi);
  }, 3000);
};

orderKopi((es) => {
  console.log(es);
});

/* output
Sedang membuat kopi, silakan tunggu...
---- setelah 3 detik ----
Kopi sudah jadi!
*/

