// HelloWorld.js
const http = require("http");
const mariadb = require("mariadb");

// const conn = mariadb.createPool({
// const conn = mariadb.createConnection({
//   host: "db2019.if.unismuh.ac.id",
//   port: "3318",
//   user: "root",
//   password: "if2019",
//   database: "latihan",
//   connectionLimit: 5,
// });

// const conn = mariadb.createConnection('mariadb://root:if2019@db2019.if.unismuh.ac.id:3318/latihan');
// const conn =  mariadb.createConnection('mariadb://root:if2019@db2019.if.unismuh.ac.id:3318/latihan');

// const getConnect = async () => {
//   try {
//     const pool = await mariadb.createPool({
//       host: "db2019.if.unismuh.ac.id",
//       port: "3318",
//       user: "root",
//       password: "if2019",
//       database: "latihan",
//       connectionLimit: 5,
//     });
//     return pool;
//   } catch (err) {
//     throw err;
//   }
// };

const pool = mariadb.createPool({
  host: "db2019.if.unismuh.ac.id",
  port: "3318",
  user: "root",
  password: "if2019",
  database: "latihan",
  connectionLimit: 5,
});

// const getConnect = async () => {
//    try {
//       const conn =  await mariadb.createConnection('mariadb://root:if2019@db2019.if.unismuh.ac.id:3318/latihan');
//       return conn
//    } catch (error) {
//       return error
//    }
// }
// const conn =  await mariadb.createConnection('mariadb://root:if2019@db2019.if.unismuh.ac.id:3318/latihan');
const mahasiswaFindAll = async () => {
  let conn;
  try {
    conn = await pool.getConnection();
    const rows = await conn.query(`SELECT nim, nama FROM mahasiswa LIMIT 5`);
    return rows;
  } catch (err) {
    throw err;
  } finally {
    if (conn) conn.release(); //release to pool
  }
};

const mahasiswaFindOne = async (nim) => {
  let conn;
  try {
    conn = await pool.getConnection();
    const rows = await conn.query("SELECT * FROM mahasiswa WHERE nim=?", [nim]);
    console.log(rows[0]);
    return rows[0];
  } catch (err) {
    throw err;
  } finally {
    if (conn) conn.release(); //release to pool
  }
};

const mahasiswaFind = async (jenis_kelaminim, kode_provinsi) => {
  let conn;
  try {
    conn = await pool.getConnection();
    const q = "SELECT * FROM mahasiswa WHERE jenis_kelamin=? AND kode_provinsi=?";
    const rows = await conn.query(q, [jenis_kelaminim, kode_provinsi]);
    console.log(rows[0]);
    return rows[0];
  } catch (err) {
    throw err;
  } finally {
    if (conn) conn.release(); //release to pool
  }
};

const server = http.createServer(async (request, response) => {
  const mahasiswa = await mahasiswaFindAll();

  // const mahasiswa = await mahasiswaFindOne("105841102819");
  // const mahasiswa = await mahasiswaFindOne('105841100419');
  // console.log(mahasiswa);

  // Send the HTTP header
  // HTTP Status: 200 : OK
  response.statusCode = 200;
  // Content Type: text/plain
  response.setHeader("Content-Type", "text/plain");
  // Send the response body as "Hello World"
  // const head = request.rawHeaders
  // response.write(`Hello ${mahasiswa.nama} (${mahasiswa.nim})`);
  mahasiswa.map((elemen) => {
    response.write(`\nNIM: ${elemen.nim}`);
    response.write(`\nNama: ${elemen.nama}`);
  });

  // response.write(`\nNIM: ${mahasiswa.nim}`);
  // response.write(`\nNama: ${mahasiswa.nama}`);
  // response.write(`\nAsal Provinsi : ${mahasiswa.provinsi}`);

  response.end();
});

server.listen(4004, () => {
  // Console will print the message
  console.log("Server running at  http://localhost:4004/");
});
