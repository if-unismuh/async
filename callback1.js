// callback1.js
const orderKopi = () => {
  let kopi = null;
  console.log("Sedang membuat kopi, silakan tunggu...");
  setTimeout(() => {
    kopi = "Kopi sudah jadi!";
  }, 3000);
  return kopi;
};

const kopi = orderKopi();
console.log(kopi);

/* output
    Sedang membuat kopi, silakan tunggu...
    null
*/
